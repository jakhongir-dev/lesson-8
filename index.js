const timer = document.querySelector(".timer");
const btnStart = document.querySelector(".btn-start");
const btnAdd = document.querySelector(".btn-add");
const btnPause = document.querySelector(".btn-pause");
const btnReset = document.querySelector(".btn-reset");
const listContainer = document.querySelector(".list-container");
const listBox = document.querySelector(".list-box");

btnAdd.disabled = true;
btnReset.disabled = true;

listBox.remove();

let sec = 0;
let min = 0;
let interval;

const retrievedSec = localStorage.getItem("sec");
const retrievedMin = localStorage.getItem("min");

if (retrievedSec) sec = retrievedSec;
if (retrievedMin) min = retrievedMin;

function renderToDom() {
  timer.innerHTML = `${min < 10 ? "0" : ""}${min}:${sec < 10 ? "0" : ""}${sec}`;
}
renderToDom();

const countUp = function () {
  sec++;
  localStorage.setItem("sec", sec);
  renderToDom();

  if (sec > 59) {
    sec = 0;
    min++;
    localStorage.setItem("min", min);
    renderToDom();
  }
};

btnStart.addEventListener("click", function () {
  btnAdd.disabled = false;
  btnReset.disabled = false;

  clearInterval(interval);
  interval = setInterval(countUp, 1000);
  btnStart.classList.add("hidden");
  btnPause.classList.remove("hidden");
});

btnPause.addEventListener("click", function () {
  btnStart.classList.remove("hidden");
  btnPause.classList.add("hidden");
  btnAdd.disabled = true;
  clearInterval(interval);
});

btnReset.addEventListener("click", function () {
  btnStart.classList.remove("hidden");
  btnPause.classList.add("hidden");
  btnAdd.disabled = true;

  clearInterval(interval);
  localStorage.clear();
  sec = 0;
  min = 0;
  renderToDom();
  document.querySelectorAll(".list-box").forEach((el) => el.remove());
});

btnAdd.addEventListener("click", function () {
  const curState = `${min < 10 ? "0" : ""}${min}:${sec < 10 ? "0" : ""}${sec}`;
  listContainer.insertAdjacentHTML(
    "beforeend",
    `<div class="list-box">
    <p class="list__time">${curState}</p>
    <img src="delete.png" alt="delete" class="btn-delete" />
  </div>`
  );
});

document
  .querySelector(".list-container")
  .addEventListener("click", function (e) {
    const curStateItem = e.target.closest(".list-box");
    curStateItem.remove();
  });
